package com.kbana.gateway.controller;

import com.kbana.bean.BaseResponse;
import com.kbana.gateway.filter.JWTFilter;
import com.kbana.gateway.model.LoginRequest;
import com.kbana.gateway.model.LoginResponse;
import com.kbana.gateway.service.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@RestController
public class AuthenticationController {


    private AuthenticationService authenticationService;

    @Autowired
    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> auth(@RequestBody LoginRequest request) {
        LoginResponse loginResponse = authenticationService.auth(request);
        HttpHeaders headers = new HttpHeaders();
        headers.add(JWTFilter.AUTHORIZATION_HEADER, JWTFilter.BEARER + loginResponse.getJwtToken());
        return new ResponseEntity<>(BaseResponse.of(loginResponse), headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/authenticate/refresh", method = RequestMethod.POST)
    public ResponseEntity<?> auth(@RequestBody Map<String, String> body) {
        LoginResponse loginResponse = authenticationService.refresh(body.get(JWTFilter.REFRESH_TOKEN_FLAG));
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER,
                JWTFilter.                                                                                                                                                               BEARER + loginResponse.getJwtToken());
        return new ResponseEntity<>(BaseResponse.of(loginResponse), httpHeaders, HttpStatus.OK);
    }

}
