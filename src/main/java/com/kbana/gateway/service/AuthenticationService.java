package com.kbana.gateway.service;

import com.kbana.gateway.filter.JWTFilter;
import com.kbana.gateway.model.LoginRequest;
import com.kbana.gateway.model.LoginResponse;
import com.kbana.gateway.security.jwt.TokenProvider;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AuthenticationService {

    @Autowired
    TokenProvider tokenProvider;

    @Autowired
    AuthenticationManager authenticationManager;

    public LoginResponse auth(LoginRequest request) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());
        Authentication authentication = null;
        try {
            authentication = authenticationManager.authenticate(authenticationToken);
        } catch (Exception e) {
            e.printStackTrace();
        }
        SecurityContextHolder.getContext().setAuthentication(authentication);
        assert authentication != null;
        String token = tokenProvider.createToken(authentication);
        String refreshToken = tokenProvider.createRefreshToken(authentication);
        LoginResponse response = LoginResponse.builder()
                .jwtToken(token)
                .refreshToken(refreshToken)
                .build();
        log.debug("login success, create jwt token success");
        return response;
    }

    public LoginResponse refresh(String token) {
        TokenProvider tokenProvider = new TokenProvider();
        if (!tokenProvider.validateToken(token)) {
            throw new BadCredentialsException("invalid token");
        }
        Claims claims = tokenProvider.getClaims(token);
        if (!claims.containsKey(JWTFilter.REFRESH_TOKEN_FLAG)) {
            throw new BadCredentialsException("invalid token: not a refresh token");
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String newToken = tokenProvider.createToken(authentication);
        String refreshToken = tokenProvider.createRefreshToken(authentication);
        LoginResponse response = LoginResponse.builder()
                .jwtToken(newToken)
                .refreshToken(refreshToken)
                .build();
        log.debug("refresh token success");
        return response;
    }
}
