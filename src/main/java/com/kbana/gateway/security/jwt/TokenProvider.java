package com.kbana.gateway.security.jwt;

import com.kbana.gateway.filter.JWTFilter;
import com.kbana.postgres.entity.KUser;
import com.kbana.postgres.repository.UserRepository;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class TokenProvider {

    private Key key;

    @Value("${mms.jwt.secret}")
    String secret;

    @Value("${mms.jwt.tokenValidityInMilliseconds}")
    long tokenValidityInMilliseconds;

    @Value("${mms.jwt.tokenValidityInMillisecondsForRememberMe}")
    long tokenValidityInMillisecondsForRememberMe;

    @Value("${mms.jwt.tokenValidityInMillisecondsForRefreshToken}")
    long tokenValidityInMillisecondsForRefreshToken;

    @Autowired
    UserRepository userRepository;

    @PostConstruct
    public void init() {
        byte[] keyBytes;
        keyBytes = secret.getBytes(StandardCharsets.UTF_8);
        this.key = Keys.hmacShaKeyFor(keyBytes);
    }

    public String createToken(Authentication authentication) {
        Date cre = new Date();
        long now = cre.getTime();
        Date validity = new Date(now + this.tokenValidityInMilliseconds);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        Map<String, String> map = new HashMap<>();
        map.put("username",userDetails.getUsername());
        return Jwts.builder()
                .setClaims(map)
                .setSubject(authentication.getName())
                .setIssuedAt(cre)
                .setExpiration(validity)
                .signWith(key, SignatureAlgorithm.HS512)
                .compact();
    }

    public String createRefreshToken(Authentication authentication) {
        Date cre = new Date();
        long now = cre.getTime();
        Date validity = new Date(now + this.tokenValidityInMillisecondsForRefreshToken);
        return Jwts.builder()
                .setSubject(authentication.getName())
                .setIssuedAt(cre)
                .claim(JWTFilter.REFRESH_TOKEN_FLAG, true)
                .signWith(key, SignatureAlgorithm.HS512)
                .setExpiration(validity)
                .compact();
    }

    public Authentication getAuthentication(String token) {
        Claims claims = Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
        User principal = new User(claims.getSubject(), "", Collections.emptyList());
        return new UsernamePasswordAuthenticationToken(principal, token, Collections.emptyList());
    }

    public boolean validateToken(String token) {
        try {
            Claims claims = Jwts.parserBuilder()
                    .setSigningKey(key)
                    .build()
                    .parseClaimsJws(token)
                    .getBody();
            KUser user = userRepository.findByUsername((String) claims.get("username"));
            return !user.getLastChangePassword().after(claims.getIssuedAt());
        } catch (JwtException | IllegalArgumentException e) {
            log.info("Invalid JWT token.");
            log.trace("Invalid JWT token trace.", e);
        }
        return false;
    }

    public Claims getClaims(String authToken) {
        try {
            Jws<Claims> claimsJwts = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(authToken);
            return claimsJwts.getBody();
        } catch (JwtException | IllegalArgumentException e) {
            log.info("Invalid JWT token.");
            log.trace("Invalid JWT token trace.", e);
        }
        return null;
    }



}
