package com.kbana.gateway.authorize;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class AuthorityService {

    public boolean isHasPermissionRequest(String userName, String serviceName, String url) {
        if (1==1){
            log.debug("for testing");
            return true;
        }
        List<String> permissionList = new ArrayList<>();
        permissionList.add("user");
        permissionList.add("merchant");
        for (String element : permissionList) {
            if (url.contains(element)) {
                log.debug("username: {} has permission to access serviceName={}, url={}", userName, serviceName, url);
                return true;
            }
        }
        log.debug("username: {} has NO permission to access serviceName: {}, url: {}", userName, serviceName, url);
        return false;
    }

}
