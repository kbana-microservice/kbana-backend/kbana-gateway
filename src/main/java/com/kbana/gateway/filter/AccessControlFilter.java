package com.kbana.gateway.filter;

import com.google.common.io.CharStreams;
import com.kbana.gateway.authorize.AuthorityService;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.zuul.filters.Route;
import org.springframework.cloud.netflix.zuul.filters.RouteLocator;
import org.springframework.context.annotation.Configuration;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

@Slf4j
@Configuration
public class AccessControlFilter extends ZuulFilter {

    private final RouteLocator routeLocator;

    public AccessControlFilter(RouteLocator routeLocator) {
        this.routeLocator = routeLocator;
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        String requestUri = RequestContext.getCurrentContext().getRequest().getRequestURI();
        String contextPath = RequestContext.getCurrentContext().getRequest().getContextPath();
        String method = RequestContext.getCurrentContext().getRequest().getMethod();
        for (Route route : routeLocator.getRoutes()) {
            String serviceUrl = contextPath + route.getFullPath();
            String serviceName = route.getId();
            if (requestUri.startsWith(serviceUrl.substring(0, serviceUrl.length() - 2))) {
                AuthorityService authorityService = new AuthorityService();
                return !authorityService.isHasPermissionRequest("John", serviceName, requestUri);
            }
        }
        return true;

    }

    @Override
    public Object run() throws ZuulException {
        RequestContext context = RequestContext.getCurrentContext();
        context.setResponseStatusCode(200);
        context.setSendZuulResponse(false);
        String s = "{\n" +
                "    \"code\": \"96\",\n" +
                "    \"message\": \"ban khong the truy cap resource nay\"\n" +
                "}";
        try (final InputStream stream = new ByteArrayInputStream(s.getBytes(StandardCharsets.UTF_8))) {
            String responseData = CharStreams.toString(new InputStreamReader(stream, StandardCharsets.UTF_8));
            context.getResponse().setHeader("Content-Type", "application/json");
            context.setResponseBody(responseData);
        } catch (Exception e) {
            throw new ZuulException(e, 1, e.getMessage());
        }
        return null;

    }
}
