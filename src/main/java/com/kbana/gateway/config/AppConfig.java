package com.kbana.gateway.config;

import com.kbana.postgres.PostgresDatabaseConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig extends PostgresDatabaseConfig {
}
