package com.kbana.gateway.security;

import com.kbana.postgres.entity.KUser;
import com.kbana.postgres.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String u) throws UsernameNotFoundException {
        KUser user = userRepository.findByUsername(u);

        if(user == null) {
            throw new UsernameNotFoundException(String.format("The username %s doesn't exist", u));
        }

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), new ArrayList<>());
    }
}
